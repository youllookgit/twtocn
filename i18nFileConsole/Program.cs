﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using i18nFileConsole.Library;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.Net;
using System.IO;
using System.Security.Cryptography;
using System.Web;
using Newtonsoft.Json.Linq;
using ExcelDataReader;
using System.Data;
using Microsoft.International.Converters.TraditionalChineseToSimplifiedConverter;

namespace i18nFileConsole
{
    
    class Program
    {
       
        public static string rootpath = FileLib.rootPath;

        static void Main(string[] args)
        {
            List<string> AllFile = new List<string>();
            #region 讀取指定資料夾所有檔案路徑
            string directFolder = FileLib.rootPath;
            var file = Directory.GetFiles(directFolder, "*", SearchOption.AllDirectories);
            //只取html&ts
            foreach (string path in file)
            {
                string filename = Path.GetExtension(path);
                if (filename == ".html" || filename == ".ts")
                {
                    AllFile.Add(path);
                }
            }
            #endregion
            foreach (var _path in AllFile)
            {
                string nowPath = _path;
                List<string> newContent = new List<string>();
                //step 1 讀檔
                var content = FileLib.ReadFileToList(nowPath);
                //step 2 逐行取代
                foreach (string line in content)
                {
                    var newline = ChineseConverter.Convert(line, ChineseConversionDirection.TraditionalToSimplified);
                    newContent.Add(newline);
                }
                var outputPath = nowPath.Replace("source", "output");
                FileLib.WriteOverFileNoBom(outputPath, newContent);
                Console.WriteLine("已完成 " + outputPath);
            }
            

            Console.WriteLine("全部完成");
            Console.Read();
        }


    }

}